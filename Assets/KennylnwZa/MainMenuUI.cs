using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    public string gameplaySceneName;

    public GameObject credit;
    public void LoadGameplayScene()
    {
        SceneManager.LoadScene(gameplaySceneName);
    }

    public void Credit()
    {
        if (!credit.activeSelf)
        {
            credit.SetActive(true);
        }
        else credit.SetActive(false);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
