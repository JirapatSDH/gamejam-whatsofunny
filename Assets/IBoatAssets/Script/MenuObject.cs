using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Menu", menuName = "MenuNumber")]
public class MenuObject : ScriptableObject
{
    public string MenuName;
    public int IngredientNumber;
    public int ProcessNumber;
    public int CookingNumber;
    public int MenuValue;
}
