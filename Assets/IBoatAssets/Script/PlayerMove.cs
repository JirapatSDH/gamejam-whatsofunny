using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMove : MonoBehaviour
{
    #region Singleton

    private static PlayerMove _instance;
    
    public static PlayerMove Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<PlayerMove>();

                if (_instance == null)
                {
                    GameObject singletonObject = new GameObject("_PlayerCapsule"); // Change the name to "_PlayerCapsule"
                    _instance = singletonObject.AddComponent<PlayerMove>();
                }
            }

            return _instance;
        }
    }
    #endregion
    
    [Header("ไปปรับใน GlobalManager")]
    [SerializeField] private float _Speed;
    private Camera _mainCamera;
    private Vector3 target;
    public SoundManager soundManager;
    public bool isGetingredient;
    public bool isGetProcess;
    public bool isGetCooking;
    public bool toPoint;
    public int _playerIngredientNumber;
    public int _playerProcessNumber;
    public int _playerCookingNumber;

    public GameObject[] ingredientPointSet;
    public GameObject[] processPointSet;
    public GameObject[] cookingPointSet;
    public GameObject[] checkPointSet;
    [SerializeField]private int getFoodNum;
    private Rigidbody2D rb;
    public int selectTarget;
    public int playerDir;
    public Animator animator;
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
        _mainCamera = Camera.main;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _Speed = GlobalManager.Instance.playerSpeed;
        transform.position = Vector3.MoveTowards(transform.position,target,_Speed*Time.deltaTime);
        target.z = 0;
        animator.SetInteger("p_Direction",playerDir);
         if (transform.position == target )
        {
            if (rb.velocity.magnitude <= 0.1f && toPoint == true)
            {
                if (_playerIngredientNumber == 0 && _playerProcessNumber == 0 && _playerCookingNumber == 0)
                {
                    if (target == ingredientPointSet[0].transform.position)
                    {
                        if (ShopControl.Instance.flourInt >= 1)
                        {
                            ShopControl.Instance.flourInt -= 1;
                            _playerIngredientNumber = getFoodNum;
                            isGetingredient = true;
                        }
                    }

                    if (target == ingredientPointSet[1].transform.position)
                    {
                        if (ShopControl.Instance.meatInt >= 1)
                        {
                            ShopControl.Instance.meatInt -= 1;
                            _playerIngredientNumber = getFoodNum;
                            isGetingredient = true;
                        }
                    }
                    if (target == ingredientPointSet[2].transform.position)
                    {
                        if (ShopControl.Instance.vegetableInt >= 1)
                        {
                            ShopControl.Instance.vegetableInt -= 1;
                            _playerIngredientNumber = getFoodNum;
                            isGetingredient = true;
                        }
                    }
                }

                if (_playerIngredientNumber != 0 && _playerProcessNumber == 0 && _playerCookingNumber == 0 )
                {
                    if (target == processPointSet[0].transform.position)
                    {
                        _playerProcessNumber = getFoodNum;
                        isGetingredient = false;
                        isGetProcess = true;
                    }
                    if (target == processPointSet[1].transform.position)
                    {
                        _playerProcessNumber = getFoodNum;
                        isGetingredient = false;
                        isGetProcess = true;
                    }
                    if (target == processPointSet[2].transform.position)
                    {
                        _playerProcessNumber = getFoodNum;
                        isGetingredient = false;
                        isGetProcess = true;
                    }
                }

                if (_playerIngredientNumber != 0 && _playerProcessNumber != 0 && _playerCookingNumber == 0)
                {
                    if (target == cookingPointSet[0].transform.position)
                    {
                        _playerCookingNumber = getFoodNum;
                        isGetProcess = false;
                        isGetCooking = true;
                    }
                    if (target == cookingPointSet[1].transform.position)
                    {
                        _playerCookingNumber = getFoodNum;
                        isGetProcess = false;
                        isGetCooking = true;
                    }
                }
                
            }
            toPoint = false;
            if (target == ingredientPointSet[0].transform.position || target == ingredientPointSet[1].transform.position || target == ingredientPointSet[2].transform.position && isGetingredient == false)
            {
                if (isGetingredient == true && _playerIngredientNumber != 0)
                {
                    switch (_playerIngredientNumber)
                    {
                        case 1:Debug.Log("This is ingredient 1");
                            break;
                        case 2:Debug.Log("This is ingredient 2");
                            break;
                        case 3:Debug.Log("This is ingredient 3");
                            break;
                    }
                }
            }
            if (target == processPointSet[0].transform.position || target == processPointSet[1].transform.position || target == processPointSet[2].transform.position)
            {
                isGetingredient = false;
            }
            if (target == cookingPointSet[0].transform.position || target == cookingPointSet[1].transform.position )
            {
                isGetingredient = false;
            }
        }

         Vector2 playerToTarget = target - transform.position;
         float angle = Vector2.SignedAngle(transform.up, playerToTarget);
         if (Mathf.Abs(angle) < 45f)
         {
             //Up
             playerDir = 1;
         }
         else if (Mathf.Abs(angle) > 135f)
         {
             //Down
             playerDir = 2;
         }
         else if (angle < 0)
         {
             //right
             playerDir = 3;
         }
         else
         {
             //left
             playerDir = 4;
         }

         if (playerToTarget == Vector2.zero)
         {
             playerDir = 0;
         }
    }
    public void Move(InputAction.CallbackContext context)
    {
       if(!context.started) return;
       target = _mainCamera.ScreenToWorldPoint(Mouse.current.position.ReadValue());
       var rayHit = Physics2D.GetRayIntersection(_mainCamera.ScreenPointToRay(Mouse.current.position.ReadValue()));
       if (!rayHit.collider) return;
       var obj = rayHit.collider.gameObject.GetComponent<FoodType>();
       if (rayHit.collider.gameObject.name != "IngredientContainer")
       {
           if (obj._Type == Type.Ingredient)
           {
               toPoint = true;
               if (rayHit.collider.gameObject.name == "TrigIngredient (1)")
               {
                   target = ingredientPointSet[0].transform.position;
                   soundManager.PlaySoundEffect();
                   getFoodNum = obj.number;
                   Debug.Log(obj.number);
               }else if (rayHit.collider.gameObject.name == "TrigIngredient (2)")
               {
                   target = ingredientPointSet[1].transform.position;
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   Debug.Log(obj.number);
               }else if (rayHit.collider.gameObject.name == "TrigIngredient (3)")
               {
                   target = ingredientPointSet[2].transform.position;
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   Debug.Log(obj.number);
               }
           }
           if (obj._Type == Type.Process)
           {
               toPoint = true;
               if (rayHit.collider.gameObject.name == "TrigPr (1)")
               {
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   target = processPointSet[0].transform.position;
               }else if (rayHit.collider.gameObject.name == "TrigPr (2)")
               {
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   target = processPointSet[1].transform.position;
               }else if (rayHit.collider.gameObject.name == "TrigPr (3)")
               {
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   target = processPointSet[2].transform.position;
               }
           }
           if (obj._Type == Type.Cooking)
           {
               toPoint = true;
               if (rayHit.collider.gameObject.name == "TrigCook (1)")
               {
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   target = cookingPointSet[0].transform.position;
               }else if (rayHit.collider.gameObject.name == "TrigCook (2)")
               {
                   getFoodNum = obj.number;
                   soundManager.PlaySoundEffect();
                   target = cookingPointSet[1].transform.position;
               }
           }
           if (obj._Type == Type.Checker)
           {
               toPoint = true;
               if (rayHit.collider.gameObject.name == "CheckPoint (1)")
               {
                   selectTarget = 1;
                   soundManager.PlaySoundEffect();
                   target = checkPointSet[0].transform.position;
               }else if (rayHit.collider.gameObject.name == "CheckPoint (2)")
               {
                   selectTarget = 2;
                   soundManager.PlaySoundEffect();
                   target = checkPointSet[1].transform.position;
               }else if (rayHit.collider.gameObject.name == "CheckPoint (3)")
               {
                   selectTarget = 3;
                   soundManager.PlaySoundEffect();
                   target = checkPointSet[2].transform.position;
               }else if (rayHit.collider.gameObject.name == "CheckPoint (4)")
               {
                   selectTarget = 4;
                   soundManager.PlaySoundEffect();
                   target = checkPointSet[3].transform.position;
               }else if (rayHit.collider.gameObject.name == "CheckPoint (5)")
               {
                   selectTarget = 5;
                   soundManager.PlaySoundEffect();
                   target = checkPointSet[4].transform.position;
               }
           }
           
       }
    }
}
